package be.vdab.ExtraH5.oef3;

import java.util.Formatter;

public class Main {
    private static Formatter formatter = new Formatter();

    public static void main(String[] args) {

        for(char i = 'A'; i <= 'z'; i++) {
            int ascii = (int) i;
            String binary = Integer.toBinaryString(i);
            String hex = Integer.toHexString(i);

            formatter.format(i + " = " + ascii + " in ASCII, " + binary + " in binary and " + hex + " in hexadecimal\n");
        }

        System.out.println(formatter.toString());
    }
}